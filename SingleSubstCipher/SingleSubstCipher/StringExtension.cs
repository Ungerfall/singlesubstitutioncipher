﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SingleSubstCipher
{
    public static class StringExtension
    {
        public static string ExceptChars(this string str, IEnumerable<char> toExclude)
        {
            var sb = new StringBuilder(str.Length);
            foreach (var c in str.Where(c => !toExclude.Contains(c)))
                sb.Append(c);
            return sb.ToString();
        }

        public static IEnumerable<string> ChunksUpto(this string str, int maxChunkSize)
        {
            if (maxChunkSize == 0)
                maxChunkSize = 1;
            var returnValue = new List<string>();
            for (var i = 0; i < str.Length; i += maxChunkSize)
                returnValue.Add(str.Substring(i, Math.Min(maxChunkSize, str.Length - i)));

            return returnValue;
        }

        public static string ToRectangleString(this string str, int columns)
        {
            if (columns == 0)
                columns = 1;
            var rows = (int)Math.Ceiling((double)str.Length / columns);
            return str.Length == rows*columns ? 
                str : 
                string.Concat(str, new string(' ', rows*columns - str.Length));
        }

        public static char[][] ToCharRectangleArray(this string message, int columns)
        {
            if (columns == 0)
                columns = 1;
            var chunkString = message.ToRectangleString(columns).ChunksUpto((int)Math.Ceiling((double)message.Length/columns));
            var enumerable = chunkString as string[] ?? chunkString.ToArray();
            var rows = enumerable.Count();
            var returnCharArray = new Char[rows][];
            var i = 0;
            foreach (var item in enumerable)
            {
                returnCharArray[i] = item.ToCharArray();
                i++;
            }

            return returnCharArray;
        }
    }
}