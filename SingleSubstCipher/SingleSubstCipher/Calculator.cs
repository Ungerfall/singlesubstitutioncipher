﻿using System;

namespace SingleSubstCipher
{
    public class Calculator
    {
        public static UInt32 RoundUpNextPower2(UInt32 number)
        {
            var n = number;
            n--;
            n |= n >> 1;
            n |= n >> 2;
            n |= n >> 4;
            n |= n >> 8;
            n |= n >> 16;
            n++;
            return n;
        }
    }
}