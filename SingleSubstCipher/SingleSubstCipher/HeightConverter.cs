﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SingleSubstCipher
{
    public class HeightConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var count = (int)values[1];
            var height = (double) values[0] - 1.4*(count + 2);
            return height/count;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}