﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SingleSubstCipher
{
    public class SingleSubstitutionKeyCipher : IKeyCipher
    {
        private List<SingleSubKeyChiperColumn> matrix;

        public string Encrypt(string message, string key)
        {
            if (key.Length <= 1) 
                return message;
            matrix = new List<SingleSubKeyChiperColumn>();
            var columns = key.Length;
            var rectangleMessage = message.ToRectangleString(key.Length);
            var rows = rectangleMessage.Length/columns;

            for (var i = 0; i < columns; ++i)
                matrix.Add(new SingleSubKeyChiperColumn
                {
                    Index = i,
                    Key = key[i],
                    Text = rectangleMessage.Substring(i * rows, rows)
                });

            return Extensions
                .StringBuilderChars(matrix
                    .OrderBy(ob => ob.Key)
                    .Select(x => x.Text)
                    .Transpose());
        }

        public string Decrypt(string message, string key)
        {
            if (key.Length <= 1)
                return message;
            matrix = new List<SingleSubKeyChiperColumn>();
            var pairs = new Tuple<int, char>[key.Length];
            for (int i = 0; i < key.Length; i++)
                pairs[i] = new Tuple<int, char>(i, key[i]);
            pairs = pairs.OrderBy(x => x.Item2).ToArray();
            for (int i = 0; i < key.Length; i++)
            {
                var text = new StringBuilder();
                for (int j = 0; j < message.Length/key.Length; j++)
                {
                    text.Append(message[i + j*key.Length]);
                }
                matrix.Add(new SingleSubKeyChiperColumn
                {
                    Index = pairs[i].Item1,
                    Key = pairs[i].Item2,
                    Text = text.ToString()
                });
            }

            return String.Concat(matrix
                .OrderBy(ob => ob.Index)
                .Select(x => x.Text));
        }
    }
}