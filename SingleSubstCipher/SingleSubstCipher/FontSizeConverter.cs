﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SingleSubstCipher
{
    public class FontSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var height = (double) value;

            if (double.IsNaN(height))
                return 8;
            else
            {
                return height*72/96;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}