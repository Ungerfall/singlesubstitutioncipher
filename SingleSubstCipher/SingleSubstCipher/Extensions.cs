﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SingleSubstCipher
{
    public static class Extensions
    {
        public static IEnumerable<IEnumerable<T>> Transpose<T>(
            this IEnumerable<IEnumerable<T>> source)
        {
            var enumerators = source.Select(e => e.GetEnumerator()).ToArray();
            try
            {
                while (enumerators.All(e => e.MoveNext()))
                {
                    yield return enumerators.Select(e => e.Current).ToArray();
                }
            }
            finally
            {
                Array.ForEach(enumerators, e => e.Dispose());
            }
        }

        public static char[][] To2DCharArray(this IEnumerable<IEnumerable<char>> source)
        {
            return source.Select(x => x.ToArray()).ToArray();
        }

        public static string StringBuilderChars(IEnumerable<IEnumerable<char>> charSequenceCollection)
        {
            var sb = new StringBuilder();
            foreach (var c in charSequenceCollection.SelectMany(charSequence =>
            {
                var enumerable = charSequence as char[] ?? charSequence.ToArray();
                return enumerable;
            }))
            {
                sb.Append(c);
            }

            return sb.ToString();
        }

        public static string ToByteString(this IEnumerable<byte> byteList)
        {
            var result = new StringBuilder();
            foreach (var item in byteList)
            {
                result.Append(string.Format("{0}-", item));
            }
            return result.ToString();
        }

        public static string BytesToString(this IEnumerable<byte> byteList)
        {
            var result = new StringBuilder();
            foreach (var item in byteList)
            {
                if (item == 0)
                    result.Append(string.Empty);
                else
                    result.Append(Convert.ToChar(item));
            }
            return result.ToString();
        }

        public static byte[] ToBytes(this uint input)
        {
            return new[]
            {
                (byte) (input & 0x000000ff),
                (byte) ((input >> 8) & 0x000000ff),
                (byte) ((input >> 16) & 0x000000ff),
                (byte) ((input >> 24) & 0x000000ff)
            };
        }

        public static uint ToDWord(this byte[] input, int offset)
        {
            if (offset + 4 > input.Length) throw new ArgumentOutOfRangeException();
            return
                (uint)
                    ((input[offset + 3] << 24) ^ (input[offset + 2] << 16) ^ (input[offset + 1] << 8) ^ (input[offset]));
        }

        public static byte[] ToBytes(this string text)
        {
            var size = text.Count(symbol => symbol == '-');
            var result = new byte[size];
            size = 0;
            var temp = new StringBuilder();

            foreach (var symbol in text)
            {
                if (symbol != '-')
                {
                    temp.Append(symbol);
                }
                else
                {
                    result[size] = byte.Parse(temp.ToString());
                    size++;
                    temp.Clear();
                }
            }

            return result;
        }
    }
}