﻿namespace SingleSubstCipher
{
    public interface IKeyCipher
    {
        string Encrypt(string message, string key);
        string Decrypt(string message, string key);
    }
}