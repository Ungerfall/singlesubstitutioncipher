﻿using System;
using System.Text;

namespace SingleSubstCipher
{
    public class Gost2814789XorFeedback : IKeyCipher
    {
        private byte[] byteKey;
        private byte[] byteResult;
        private byte[] butesString;

        public string Encrypt(string message, string key)
        {
            butesString = GetBytesArrayFromText(message);
            byteResult = new byte[butesString.Length];
            byteKey = GetKeyFromText(key);
            Gost28147Ecb(butesString, byteResult, byteKey);

            return byteResult.ToByteString();
        }

        public string Decrypt(string message, string key)
        {
            butesString = message.ToBytes();
            byteResult = new byte[butesString.Length];
            byteKey = GetKeyFromText(key);
            Gost28147EcbDecrypt(byteResult, butesString, byteKey);

            return byteResult.BytesToString();
        }

        private readonly byte[,] p =
        {
            {0x04, 0x0a, 0x09, 0x02, 0x0d, 0x08, 0x00, 0x0e, 0x06, 0x0B, 0x01, 0x0c, 0x07, 0x0f, 0x05, 0x03},
            {0x0e, 0x0b, 0x04, 0x0c, 0x06, 0x0d, 0x0f, 0x0a, 0x02, 0x03, 0x08, 0x01, 0x00, 0x07, 0x05, 0x09},
            {0x05, 0x08, 0x01, 0x0d, 0x0a, 0x03, 0x04, 0x02, 0x0e, 0x0f, 0x0c, 0x07, 0x06, 0x00, 0x09, 0x0b},
            {0x07, 0x0d, 0x0a, 0x01, 0x00, 0x08, 0x09, 0x0f, 0x0e, 0x04, 0x06, 0x0c, 0x0b, 0x02, 0x05, 0x03},
            {0x06, 0x0c, 0x07, 0x01, 0x05, 0x0f, 0x0d, 0x08, 0x04, 0x0a, 0x09, 0x0e, 0x00, 0x03, 0x0b, 0x02},
            {0x04, 0x0b, 0x0a, 0x00, 0x07, 0x02, 0x01, 0x0d, 0x03, 0x06, 0x08, 0x05, 0x09, 0x0c, 0x0f, 0x0e},
            {0x0d, 0x0b, 0x04, 0x01, 0x03, 0x0f, 0x05, 0x09, 0x00, 0x0a, 0x0e, 0x07, 0x06, 0x08, 0x02, 0x0c},
            {0x01, 0x0f, 0x0d, 0x00, 0x05, 0x07, 0x0a, 0x04, 0x09, 0x02, 0x03, 0x0e, 0x06, 0x0b, 0x08, 0x0c}
        };

        private void Gost28147Ecb(byte[] ot, byte[] st, byte[] k)
        {
            if (ot.Length != st.Length) throw new ArgumentException("Invalid input arrays length");
            if ((ot.Length%8) != 0) throw new ArgumentException("Invalid input arrays length");
            int offset = 0;
            while (offset < ot.Length)
            {
                uint tempOutH;
                uint tempOutL;
                var tempInH = ot.ToDWord(offset);
                var tempInL = ot.ToDWord(offset + 4);
                EncryptBlock(ref tempInH, ref tempInL, out tempOutH, out tempOutL, p, k);
                Array.Copy(tempOutH.ToBytes(), 0, st, offset, 4);
                Array.Copy(tempOutL.ToBytes(), 0, st, offset + 4, 4);
                offset += 8;
            }
        }

        private void Gost28147EcbDecrypt(byte[] ot, byte[] st, byte[] k)
        {
            if (ot.Length != st.Length) throw new ArgumentException("Invalid input arrays length");
            if ((st.Length%8) != 0) throw new ArgumentException("Invalid input arrays length");
            var offset = 0;
            while (offset < ot.Length)
            {
                uint tempOutH;
                uint tempOutL;
                var tempInH = st.ToDWord(offset);
                var tempInL = st.ToDWord(offset + 4);
                DecryptBlock(ref tempInH, ref tempInL, out tempOutH, out tempOutL, p, k);
                Array.Copy(tempOutH.ToBytes(), 0, ot, offset, 4);
                Array.Copy(tempOutL.ToBytes(), 0, ot, offset + 4, 4);
                offset += 8;
            }
        }

        private void DecryptBlock(ref uint inH, ref uint inL, out uint outH, out uint outL, byte[,] pi, byte[] k)
        {
            var m1 = inH;
            var m2 = inL;
            for (int offset = 0; offset < 32; offset += 4)
                GostTransform(ref m1, ref m2, pi, k.ToDWord(offset));

            for (int i = 0; i < 2; ++i)
                for (int offset = 28; offset >= 0; offset -= 4)
                    GostTransform(ref m1, ref m2, pi, k.ToDWord(offset));

            for (int offset = 28; offset >= 4; offset -= 4)
                GostTransform(ref m1, ref m2, pi, k.ToDWord(offset));

            outH = m1;
            outL = Pi(pi, m1 + k.ToDWord(0));
            outL = (outL << 11) | (outL >> 21);
            outL ^= m2;
        }

        private void EncryptBlock(ref uint inH, ref uint inL, out uint outH, out uint outL, byte[,] pi, byte[] k)
        {
            var m1 = inH;
            var m2 = inL;
            for (int i = 0; i < 3; ++i)
                for (int offset = 0; offset < 32; offset += 4)
                    GostTransform(ref m1, ref m2, pi, k.ToDWord(offset));

            for (int offset = 28; offset >= 4; offset -= 4)
                GostTransform(ref m1, ref m2, pi, k.ToDWord(offset));

            outH = m1;
            outL = Pi(pi, m1 + k.ToDWord(0));
            outL = (outL << 11) | (outL >> 21);
            outL ^= m2;
        }

        private void GostTransform(ref uint inBlockH, ref uint inBlockL, byte[,] pi, uint k)
        {
            var outBlockH = Pi(pi, inBlockH + k);
            outBlockH = (outBlockH << 11) | (outBlockH >> 21);
            outBlockH ^= inBlockL;
            var outBlockL = inBlockH;
            inBlockL = outBlockL;
            inBlockH = outBlockH;
        }

        private uint Pi(byte[,] sBox, uint inBlock)
        {
            if (sBox == null) throw new ArgumentNullException("sBox");
            uint res = 0;
            res ^= sBox[0, (inBlock & 0x0000000f)];
            res ^= (uint) (sBox[1, ((inBlock & 0x000000f0) >> 4)] << 4);
            res ^= (uint) (sBox[2, ((inBlock & 0x00000f00) >> 8)] << 8);
            res ^= (uint) (sBox[3, ((inBlock & 0x0000f000) >> 12)] << 12);
            res ^= (uint) (sBox[4, ((inBlock & 0x000f0000) >> 16)] << 16);
            res ^= (uint) (sBox[5, ((inBlock & 0x00f00000) >> 20)] << 20);
            res ^= (uint) (sBox[6, ((inBlock & 0x0f000000) >> 24)] << 24);
            res ^= (uint) (sBox[7, ((inBlock & 0xf0000000) >> 28)] << 28);
            return res;
        }

        private byte[] GetKeyFromText(string key)
        {
            var enc = new UTF8Encoding();
            var strBytes = enc.GetBytes(key);
            var bytesKey = new byte[32];
            Array.Copy(strBytes, 0, bytesKey, 0, 32);

            return bytesKey;
        }

        private byte[] GetBytesArrayFromText(string text)
        {
            int blocksCount;
            var enc = new UTF8Encoding();
            var strBytes = enc.GetBytes(text);
            var tmp = (double) strBytes.Length/8;
            if ((strBytes.Length%8) != 0)
            {
                blocksCount = Convert.ToInt32(Math.Truncate(tmp)) + 1;
            }
            else
            {
                blocksCount = Convert.ToInt32(Math.Truncate(tmp));
            }

            var bytesArray = new byte[blocksCount*8];
            Array.Copy(strBytes, 0, bytesArray, 0, strBytes.Length);
            return bytesArray;
        }
    }
}