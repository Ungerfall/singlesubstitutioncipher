﻿namespace SingleSubstCipher
{
    public class SingleSubKeyChiperColumn
    {
        public int Index { get; set; }
        public char Key { get; set; }
        public string Text { get; set; }
    }
}