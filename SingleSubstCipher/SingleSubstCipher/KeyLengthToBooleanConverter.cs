﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SingleSubstCipher
{
    public class KeyLengthToBooleanConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var length = ((string)values[0]).Trim().Length;
            var index = (int) values[1];
            return index == 0 || length >= 32;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}