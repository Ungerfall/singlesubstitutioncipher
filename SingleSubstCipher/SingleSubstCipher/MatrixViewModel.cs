﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using SingleSubstCipher.Properties;

namespace SingleSubstCipher
{
    public sealed class MatrixViewModel : INotifyPropertyChanged
    {
        private char[][] cells;

        public char[][] Cells
        {
            get { return cells; }
            set
            {
                cells = value;
                OnPropertyChanged();
            }
        }

        private char[] key;

        public char[] Key
        {
            get { return key; }
            set
            {
                key = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}