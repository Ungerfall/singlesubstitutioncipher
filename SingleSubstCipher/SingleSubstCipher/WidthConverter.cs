﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SingleSubstCipher
{
    public class WidthConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var rows = (int) values[0] + 1;
            var columns = (int)values[1];
            if (rows > columns)
                return ((double) values[2] - 1.4*(rows + 2))/rows;
            return ((double) values[3] - 1.4*(columns + 2))/columns;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}