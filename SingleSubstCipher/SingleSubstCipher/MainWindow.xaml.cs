﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SingleSubstCipher
{
    public partial class MainWindow
    {
        private readonly MainWindowViewModel windowViewModel;
        private Dictionary<int, IKeyCipher> cipher; 
        private readonly HashSet<char> excludeChars = new HashSet<char> {' ', '\t', '\n', '\r'};
        private int selectedCipher;
        private string key;
        private string message;

        public MainWindow()
        {
            InitializeComponent();
            cipher = new Dictionary<int, IKeyCipher>
            {
                {0, new SingleSubstitutionKeyCipher()},
                {1, new Gost2814789XorFeedback()}
            };
            windowViewModel = new MainWindowViewModel
            {
                CipherType = cipher.Select(x => x.Value.ToString()).ToList()
            };
            DataContext = windowViewModel;
        }

        private void EncryptButtonOnClick(object sender, RoutedEventArgs e)
        {
            key = KeyBox.Text.ExceptChars(excludeChars);
            message = MessageBox.Text.ExceptChars(excludeChars);
            selectedCipher = CipherTypeBox.SelectedIndex;
            var encryptedMessage = cipher[selectedCipher].Encrypt(message, key);
            if (selectedCipher == 0)
            {
                windowViewModel.InputMatrix = new MatrixViewModel
                {
                    Key = key.ToCharArray(),
                    Cells = message.ToCharRectangleArray(key.Length)
                };
                windowViewModel.OutputMatrix = new MatrixViewModel
                {
                    Key = key.OrderBy(c => c).ToArray(),
                    Cells = encryptedMessage
                        .ToCharRectangleArray((int)Math.Ceiling((double)message.Length / key.Length))
                        .Transpose()
                        .To2DCharArray()
                };
            }
            windowViewModel.OutputMessage = encryptedMessage;
            AnsweGrid.Visibility = Visibility.Visible;
            AnswerTextBlock.Text = "Зашифрованное сообщение";
        }

        private void DecryptButtonOnClick(object sender, RoutedEventArgs e)
        {
            key = KeyBox.Text.ExceptChars(excludeChars);
            message = MessageBox.Text.Trim();
            selectedCipher = CipherTypeBox.SelectedIndex;
            var decryptedMessage = cipher[CipherTypeBox.SelectedIndex].Decrypt(message, key);
            if (selectedCipher == 0)
            {
                windowViewModel.InputMatrix = new MatrixViewModel
                {
                    Key = key.OrderBy(c => c).ToArray(),
                    Cells = message.ToCharRectangleArray(key.Length)
                };
                windowViewModel.OutputMatrix = new MatrixViewModel
                {
                    Key = key.ToCharArray(),
                    Cells = decryptedMessage
                        .ToCharRectangleArray(key.Length)
                };
            }
            windowViewModel.OutputMessage = decryptedMessage;
            AnsweGrid.Visibility = Visibility.Visible;
            AnswerTextBlock.Text = "Дешифрованное сообщение";
        }

        private void MainWindowOnLoaded(object sender, RoutedEventArgs e)
        {
            MessageBox.Focus();
        }

        private void OnCipherTypeBoxSelected(object sender, SelectionChangedEventArgs e)
        {
            var cipherBox = sender as ComboBox;
            if (cipherBox == null) return;
            if (cipherBox.SelectedIndex != -1)
                windowViewModel.IsSelectedCipher = true;
        }
    }
}
