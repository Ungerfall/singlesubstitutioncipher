﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SingleSubstCipher
{
    public class ErrorLengthKeyConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var keyLength = ((string)values[0]).Trim().Length;
            var index = (int) values[1];
            return index == 1 && keyLength < 32;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}