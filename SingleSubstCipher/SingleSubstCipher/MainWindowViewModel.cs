﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using SingleSubstCipher.Properties;

namespace SingleSubstCipher
{
    public sealed class MainWindowViewModel : INotifyPropertyChanged
    {
        private List<string> cipherType;

        public List<string> CipherType
        {
            get { return cipherType; }
            set
            {
                cipherType = value;
                OnPropertyChanged();
            }
        }

        private bool isSelectedCipher;

        public bool IsSelectedCipher
        {
            get { return isSelectedCipher; }
            set
            {
                isSelectedCipher = value;
                OnPropertyChanged();
            }
        }

        private MatrixViewModel inputMatrix;

        public MatrixViewModel InputMatrix
        {
            get { return inputMatrix; }
            set
            {
                inputMatrix = value;
                OnPropertyChanged();
            }
        }

        private MatrixViewModel outputMatrix;

        public MatrixViewModel OutputMatrix
        {
            get { return outputMatrix; }
            set
            {
                outputMatrix = value;
                OnPropertyChanged();
            }
        }

        private string outputMessage;

        public string OutputMessage
        {
            get { return outputMessage; }
            set
            {
                outputMessage = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}