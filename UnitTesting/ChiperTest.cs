﻿using NUnit.Framework;
using SingleSubstCipher;

namespace UnitTesting
{
    public class ChiperTest :TestBase
    {
        private SingleSubstitutionKeyCipher cipher;
        protected override void Setup()
        {
            cipher = new SingleSubstitutionKeyCipher();
        }

        [Test]
        public void EncryptTest()
        {
            var text = "собака";
            var key = "пес";
            var encryptedText = cipher.Encrypt(text, key);
            Assert.AreEqual(encryptedText, "бскаоа");
        }

        [Test]
        public void DecryptTest()
        {
            var text = "олет";
            var key = "зима";
            var decryptedText = cipher.Decrypt(text, key);
            Assert.AreEqual(decryptedText, "лето");
        }

        [Test]
        public void TwoWayTest()
        {
            var text = "рубероид";
            var key = "ёжик";
            Assert.AreEqual(cipher.Decrypt(cipher.Encrypt(text, key), key), text);
        }
    }
}