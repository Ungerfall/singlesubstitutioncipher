﻿using NUnit.Framework;
using SingleSubstCipher;

namespace UnitTesting
{
    public class StringExtensionTest : TestBase
    {
        private string text;
        protected override void Setup()
        {
            
        }

        [Test]
        public void ChunkTest()
        {
            text = "1st2nd3rd";
            Assert.AreEqual(new[]{"1st","2nd","3rd"}, text.ChunksUpto(3));
        }

        [Test]
        public void To2DArrayTest()
        {
            text = "12345678";
            Assert.AreEqual(new[]
            {
                new[] {'1', '2', '3'},
                new[] {'4', '5', '6'},
                new[] {'7', '8', ' '}
            }, text.ToCharRectangleArray(3));
        }
    }
}