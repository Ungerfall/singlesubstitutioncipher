﻿using NUnit.Framework;

namespace UnitTesting
{
    public abstract class TestBase
    {
        protected abstract void Setup();

        [SetUp]
        public void SetUp()
        {
            Setup();
        }
    }
}